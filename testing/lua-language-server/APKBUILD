# Maintainer: psykose <alice@ayaya.dev>
pkgname=lua-language-server
pkgver=3.6.13
pkgrel=0
pkgdesc="Language Server for Lua"
url="https://github.com/LuaLS/lua-language-server"
# s390x/ppc64le: ftbfs
# 32-bit: not useful there
arch="all !x86 !armhf !armv7 !s390x !ppc64le"
license="MIT"
makedepends="bash samurai"
source="https://github.com/LuaLS/lua-language-server/archive/refs/tags/$pkgver/lua-language-server-$pkgver.tar.gz
	lua-language-server-submodules-$pkgver.zip.noauto::https://github.com/LuaLS/lua-language-server/releases/download/$pkgver/lua-language-server-$pkgver-submodules.zip
	wrapper
	"
options="!check" # no tests

prepare() {
	unzip -o "$srcdir"/lua-language-server-submodules-$pkgver.zip.noauto \
		-d "$builddir"
	default_prepare
}

build() {
	ninja -C 3rd/luamake -f compile/ninja/linux.ninja
	./3rd/luamake/luamake rebuild
}

package() {
	install -Dm755 "$srcdir"/wrapper "$pkgdir"/usr/bin/lua-language-server
	install -Dm755 bin/lua-language-server \
		-t "$pkgdir"/usr/lib/lua-language-server/bin
	install -Dm644 bin/main.lua \
		-t "$pkgdir"/usr/lib/lua-language-server/bin
	install -Dm644 debugger.lua main.lua \
		-t "$pkgdir"/usr/lib/lua-language-server
	cp -a locale meta script "$pkgdir"/usr/lib/lua-language-server
}

sha512sums="
7af79a60fad2e84e9cc50117b131f2c6eb73c4eca7cb7c7dce51ecb70022e6597e618dfc7b9bbf58570e79c88de5ba7089227f0a8d1adfaa7f251e09d4e75fac  lua-language-server-3.6.13.tar.gz
ce595dfe2e16bb468e90c2e73d5569eeac2cf093a0caaa0006e23ec876edcf491be92dad88e35369f9c250949c887850e8c1805736e8b8f20b0c643a56d6faed  lua-language-server-submodules-3.6.13.zip.noauto
75a65e2e084b1f8e11b88f874ad399f51dbd280c02eaa0d8aa79e7c1fdc9e734104ef4f418f733b8d4df5eadfee8683087cc3d13e783e6104c4e7ffa4671cdf3  wrapper
"
