# Contributor: Holger Jaekel <holger.jaekel@gmx.de>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=aws-c-common
pkgver=0.8.13
pkgrel=0
pkgdesc="Core c99 package for AWS SDK for C including cross-platform primitives, configuration, data structures, and error handling"
url="https://github.com/awslabs/aws-c-common"
# s390x: fails tests
arch="all !s390x"
license="Apache-2.0"
makedepends="
	cmake
	python3-dev
	samurai
	"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/awslabs/aws-c-common/archive/refs/tags/v$pkgver.tar.gz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	CFLAGS="$CFLAGS -flto=auto" \
	CXXFLAGS="$CXXFLAGS -flto=auto" \
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=None \
		-DBUILD_TESTING="$(want_check && echo ON || echo OFF)" \
		$CMAKE_CROSSOPTS
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure -j${JOBS:-2}
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

dev() {
	default_dev
	amove usr/lib/aws-c-common
}

sha512sums="
05617cd1d09a3b55066b6032e441c184d2a1643c9e5b95ade8f270254db0ed565db583d1ef34a7f308101383ce685b84d722c3d9c1933a3b81abf09cde1b0746  aws-c-common-0.8.13.tar.gz
"
