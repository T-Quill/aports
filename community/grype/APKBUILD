# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=grype
pkgver=0.59.0
pkgrel=0
pkgdesc="Vulnerability scanner for container images, filesystems, and SBOMs"
url="https://github.com/anchore/grype"
license="Apache-2.0"
# ppc64le: build constraints exclude all Go files in /home/buildozer/aports/testing/grype/src/pkg/mod/modernc.org/libc@v1.14.12/uuid/uuid
arch="all !ppc64le !armhf !armv7 !x86" # FTBFS on 32-bit arches
makedepends="go"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/anchore/grype/archive/v$pkgver/grype-$pkgver.tar.gz"
options="!check" # tests need docker

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -ldflags "
		-X github.com/anchore/grype/internal/version.version=$pkgver
		" \
		-o bin/grype

	./bin/grype completion bash > $pkgname.bash
	./bin/grype completion fish > $pkgname.fish
	./bin/grype completion zsh > $pkgname.zsh
}

check() {
	go test ./...
}

package() {
	install -Dm755 bin/grype -t "$pkgdir"/usr/bin/

	install -Dm644 $pkgname.bash "$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish "$pkgdir"/usr/share/fish/completions/$pkgname.fish
	install -Dm644 $pkgname.zsh "$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
dd04055ed749388d3d77966859bb0a0ea732479366f669cedde3db47e05839dcaefc853bd558c24a9f18632729dab24f0fea5ce213939f8f06132382070f32cc  grype-0.59.0.tar.gz
"
